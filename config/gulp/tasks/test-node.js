import gulp from 'gulp';
import mocha from 'gulp-mocha';

// Lint and run unit tests for the node.js environment
gulp.task('test:node', ['lint'], () => {
	require('babel-register');
	return gulp.src(['config/setup/node.js', 'test/node-tests/**/*node.js'], {read: false})
		.pipe(mocha({
			reporter: 'spec',
			globals: Object.keys({
				expect: true,
				mock: true,
				sandbox: true,
				spy: true,
				stub: true,
				useFakeServer: true,
				useFakeTimers: true,
				useFakeXMLHttpRequest: true
			}),
			timeout: 15000,
			ignoreLeaks: false
		}));
});