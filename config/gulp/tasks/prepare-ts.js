import gulp from 'gulp';
import rollup from 'rollup';
import typescript from 'rollup-plugin-typescript';
import replace from 'rollup-plugin-replace';
import tsConfig from '../../ts/tsconfig.json';

gulp.task('prepare:ts', () => {
	return rollup.rollup({
		entry: 'test/specs/**/*.ts',
		plugins: [
			typescript(Object.assign(tsConfig.compilerOptions, {
				target: 'es5'
			})),
			replace({
				delimiters: ['<@', '@>'],
				values: {}
			})
		]
	}).then(bundle => {
		return bundle.write({
			format: 'iife',
			dest: 'build/tests.js',
			sourceMap: 'inline'
		});
	});
});
